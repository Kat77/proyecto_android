package mx.easypaid.login

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val text_usuario = findViewById<TextView>(R.id.text_usuario)
        text_usuario.setText("Correo electrónico o nombre de usuario")

        val text_contraseña = findViewById<TextView>(R.id.text_contrasena)
        text_contraseña.setText("Contraseña")


        //Obtenemos el componente gráfico del layout. El id del botón es btn_entrar
        val boton = findViewById<Button>(R.id.btn_entrar)
        val tache = findViewById<TextView>(R.id.X_textView)
        val tache2 = findViewById<TextView>(R.id.X2_textView)

        //Asignamos un listener para el evento de OnClick al botón
        boton.setOnClickListener{

            saveData()

            //Mostramos mensaje en pantalla a través de un Toast
            if(text_usuario.text.length==0){
                Toast.makeText(this.applicationContext, "Faltan datos", Toast.LENGTH_LONG).show()
                tache.visibility=View.VISIBLE
            }else if(text_contraseña.text.length==0){
                Toast.makeText(this.applicationContext, "Faltan datos", Toast.LENGTH_LONG).show()
                tache2.visibility=View.VISIBLE
            }else{
                tache.visibility=View.INVISIBLE
                tache2.visibility=View.INVISIBLE
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }

            if(text_usuario.text.length<=5){
                Toast.makeText(this.applicationContext, "Correo electrónico inválido", Toast.LENGTH_LONG).show()
                tache.visibility=View.VISIBLE
            }

            if(text_contraseña.text.length<8){
                Toast.makeText(this.applicationContext, "Contraseña inválida", Toast.LENGTH_LONG).show()
                tache2.visibility=View.VISIBLE
            }
        }
    }

    private fun retrieveData(){
      //  val mypref = getSharedPreferences("")
    }

    private fun saveData(){
        if(text_usuario.text.isEmpty()){
            text_usuario.error = "Ingresa tu correo electrónico"
            return
        }
        if(text_contrasena.text.isEmpty()){
            text_contrasena.error = "Ingresa la contraseña"
            return
        }

        val mypref = getSharedPreferences("mypref", Context.MODE_PRIVATE)

        val editor = mypref.edit()

        editor.putString("Correo electrónico", text_usuario.text.toString())
        editor.putString("Contraseña", text_contrasena.text.toString())

        editor.apply()

        Toast.makeText(this,"Guardado", Toast.LENGTH_LONG).show()

    }

}
